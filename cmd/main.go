package main

import (
	"log"

	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	"gitlab.com/meshnyan/todo-app"
	"gitlab.com/meshnyan/todo-app/pkg/handler"
	"gitlab.com/meshnyan/todo-app/pkg/repository"
	"gitlab.com/meshnyan/todo-app/pkg/service"
)

func main() {
	if err := initConfig(); err != nil {
		log.Fatalf("Asgard's configuration has destroed with error: %s", err.Error())
	}

	db, err := repository.NewPostgresDB(repository.Config{
		Host:     viper.GetString("db.host"),
		Port:     viper.GetString("db.port"),
		Username: viper.GetString("db.user"),
		Password: viper.GetString("db.password"),
		DBName:   viper.GetString("db.name"),
		SSLMode:  viper.GetString("db.ssl-mode"),
	})
	if err != nil {
		log.Fatalf("Asgard's storage has been purged by enemy: %s", err.Error())
	}

	repos := repository.NewRepository(db)
	services := service.NewService(repos)
	handlers := handler.NewHandler(services)

	srv := new(todo.Server)
	if err := srv.Run(viper.GetString("port"), handlers.InitRoutes()); err != nil {
		log.Fatalf("Asgard has been defeated due to: %s", err.Error())
	}
}

func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}
