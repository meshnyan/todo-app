package entities

type Todo struct {
	Id     string `json:"id"`
	Desc   string `json:"desc"`
	IsDone bool   `json:"isDone"`
}
