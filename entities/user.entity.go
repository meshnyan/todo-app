package entities

type User struct {
	Id    int     `json:"id"`
	Email string  `json:"email"`
	Name  *string `json:"name"`
	Age   *int    `json:"age"`
}

type UpdateUserDTO struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}
