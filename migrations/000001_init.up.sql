create table users
(
  "id" serial not null unique,
  "email" varchar(255) not null unique,
  "password" varchar(255) not null,
  "name" varchar(255),
  "age" int,
  "created_at" timestamptz NOT NULL DEFAULT (now())
);

create table user_access_tokens
(
  "id" serial not null unique,
  "token" varchar(255) not null,
  "is_active" boolean not null default (true),
  "user_id" integer not null,
  "created_at" timestamptz NOT NULL DEFAULT (now())
);

create table todos
(
  "id" serial not null unique,
  "title" varchar(255) not null,
  "desc" varchar(255),
  "is_done" boolean not null default (false),
  "user_id" integer not null,
  "created_at" timestamptz NOT NULL DEFAULT (now())
);

alter table "user_access_tokens" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
alter table "todos" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");