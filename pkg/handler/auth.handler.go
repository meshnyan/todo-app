package handler

import (
	"database/sql"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/meshnyan/todo-app/entities"
)

func (h *Handler) signUp(ctx *gin.Context) {

	var auth entities.AuthRequestDTO
	if err := ctx.BindJSON(&auth); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, err.Error())
		return
	}

	token, err := h.services.Auth.SignUp(auth)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, map[string]interface{}{
		"token": token,
	})
}

func (h *Handler) signIn(ctx *gin.Context) {
	var auth entities.AuthRequestDTO
	if err := ctx.BindJSON(&auth); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, err.Error())
		return
	}

	token, err := h.services.Auth.SignIn(auth)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.AbortWithStatusJSON(http.StatusNotFound, "User not found")
		} else {
			ctx.AbortWithStatusJSON(http.StatusInternalServerError, err.Error())
		}
		return
	}

	ctx.JSON(http.StatusOK, map[string]interface{}{
		"token": token,
	})
}
