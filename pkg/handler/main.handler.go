package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/meshnyan/todo-app/pkg/middleware"
	"gitlab.com/meshnyan/todo-app/pkg/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()
	middleware := middleware.NewMiddleware(h.services)
	auth := router.Group("/auth")
	{
		auth.POST("/sign-in", h.signIn)
		auth.POST("/sign-up", h.signUp)
	}
	api := router.Group("/api")
	api.Use(middleware.Authenticate())
	{
		user := api.Group("/user")
		{
			user.GET("/me", h.getMe)
			user.PATCH("/me", h.updateMe)
		}
		todo := api.Group("/todo")
		{
			todo.POST("/", h.addTodo)
			todo.GET("list", h.getTodos)
			todo.GET("/:id", h.getTodo)
			todo.PATCH("/:id", h.updateTodo)
			todo.DELETE("/:id", h.deleteTodo)
		}
	}
	return router
}
