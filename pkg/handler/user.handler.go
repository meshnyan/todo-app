package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/meshnyan/todo-app/entities"
)

func (h *Handler) getMe(ctx *gin.Context) {
	user, err := h.services.User.GetUserById(ctx.GetInt("userId"))
	if err != nil {
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	ctx.JSON(http.StatusOK, user)
}

func (h *Handler) updateMe(ctx *gin.Context) {
	var dto entities.UpdateUserDTO
	if err := ctx.BindJSON(&dto); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, err.Error())
		return
	}
	err := h.services.User.UpdateUserById(ctx.GetInt("userId"), dto)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, "ok")
}
