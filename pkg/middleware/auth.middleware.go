package middleware

import (
	"database/sql"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/meshnyan/todo-app/pkg/service"
)

type AuthMiddleware struct {
	service service.Auth
}

func NewAuthMiddleware(service service.Auth) *AuthMiddleware {
	return &AuthMiddleware{service: service}
}

func (m *AuthMiddleware) Authenticate() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		token := ctx.GetHeader("Authorization")
		if token == "" {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		userId, err := m.service.AuthenticateByToken(token)

		if err != nil {
			if err == sql.ErrNoRows {
				ctx.AbortWithStatus(http.StatusUnauthorized)
				return
			} else {
				ctx.AbortWithStatus(http.StatusInternalServerError)
				return
			}
		}

		ctx.Set("userId", userId)
	}
}
