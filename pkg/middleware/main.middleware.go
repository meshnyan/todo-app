package middleware

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/meshnyan/todo-app/pkg/service"
)

type Auth interface {
	Authenticate() gin.HandlerFunc
}

type Middleware struct {
	Auth
}

func NewMiddleware(service *service.Service) *Middleware {
	return &Middleware{
		Auth: NewAuthMiddleware(service.Auth),
	}
}
