package repository

import (
	"fmt"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/meshnyan/todo-app/entities"
)

type AuthRepository struct {
	db *sqlx.DB
}

func NewAuthRepository(db *sqlx.DB) *AuthRepository {
	return &AuthRepository{db: db}
}

func (r *AuthRepository) SignUp(dto entities.AuthRequestDTO) (string, error) {
	var userId int
	query := fmt.Sprintf("insert into %s (email, password) values ($1, $2) returning id", USERS_TABLE)
	row := r.db.QueryRow(query, dto.Email, dto.Password)
	if err := row.Scan(&userId); err != nil {
		return "", err
	}
	token := uuid.NewString()
	query = fmt.Sprintf("insert into %s (token, user_id) values ($1, $2) returning token", UAT_TABLE)
	row = r.db.QueryRow(query, token, userId)
	if err := row.Scan(&token); err != nil {
		return "", err
	}

	return token, nil
}

func (r *AuthRepository) SignIn(dto entities.AuthRequestDTO) (token string, err error) {
	var userId int
	query := fmt.Sprintf("select id from %s where email = $1 and password = $2", USERS_TABLE)
	row := r.db.QueryRow(query, dto.Email, dto.Password)
	if err = row.Scan(&userId); err != nil {
		return "", err
	}

	token = uuid.NewString()
	query = fmt.Sprintf("insert into %s (token, user_id) values ($1, $2) returning token", UAT_TABLE)
	row = r.db.QueryRow(query, token, userId)
	if err := row.Scan(&token); err != nil {
		return "", err
	}

	return token, nil
}

func (r *AuthRepository) AuthenticateByToken(token string) (int, error) {
	var userId int
	query := fmt.Sprintf("select user_id from %s where token = $1", UAT_TABLE)
	row := r.db.QueryRow(query, token)
	if err := row.Scan(&userId); err != nil {
		return 0, err
	}
	return userId, nil
}
