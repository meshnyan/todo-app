package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/meshnyan/todo-app/entities"
)

type Auth interface {
	SignUp(auth entities.AuthRequestDTO) (string, error)
	SignIn(auth entities.AuthRequestDTO) (string, error)
	AuthenticateByToken(token string) (int, error)
}

type User interface {
	GetUserById(id int) (entities.User, error)
	UpdateUserById(id int, dto entities.UpdateUserDTO) error
}

type Todo interface {
}

type Repository struct {
	Auth
	User
	Todo
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Auth: NewAuthRepository(db),
		User: NewUserRepository(db),
	}
}
