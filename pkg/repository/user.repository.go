package repository

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/meshnyan/todo-app/entities"
)

type UserRepository struct {
	db *sqlx.DB
}

func NewUserRepository(db *sqlx.DB) *UserRepository {
	return &UserRepository{db: db}
}

func (r *UserRepository) GetUserById(id int) (entities.User, error) {
	var user entities.User
	query := fmt.Sprintf("select id, email, name, age from %s where id = $1", USERS_TABLE)
	row := r.db.QueryRow(query, id)
	if err := row.Scan(&user.Id, &user.Email, &user.Name, &user.Age); err != nil {
		return user, err
	}
	return user, nil
}

func (r *UserRepository) UpdateUserById(id int, dto entities.UpdateUserDTO) error {
	query := fmt.Sprintf("update %s set name=$1, age=$2 where id=$3", USERS_TABLE)
	_, err := r.db.Exec(query, dto.Name, dto.Age, id)
	return err
}
