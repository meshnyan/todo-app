package service

import (
	"crypto/sha1"
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/meshnyan/todo-app/entities"
	"gitlab.com/meshnyan/todo-app/pkg/repository"
)

type AuthService struct {
	repo repository.Auth
}

func NewAuthService(repo repository.Auth) *AuthService {
	return &AuthService{repo: repo}
}

func (s *AuthService) SignUp(dto entities.AuthRequestDTO) (string, error) {
	dto.Password = s.generatePasswordHash(dto.Password)
	return s.repo.SignUp(dto)
}

func (s *AuthService) SignIn(dto entities.AuthRequestDTO) (string, error) {
	dto.Password = s.generatePasswordHash(dto.Password)
	return s.repo.SignIn(dto)
}

func (s *AuthService) generatePasswordHash(password string) string {
	hash := sha1.New()
	hash.Write([]byte(password))

	return fmt.Sprintf("%x", hash.Sum([]byte(viper.GetString("password_salt"))))
}

func (s *AuthService) AuthenticateByToken(token string) (int, error) {
	return s.repo.AuthenticateByToken(token)
}
