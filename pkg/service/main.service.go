package service

import (
	"gitlab.com/meshnyan/todo-app/entities"
	"gitlab.com/meshnyan/todo-app/pkg/repository"
)

type Auth interface {
	SignUp(auth entities.AuthRequestDTO) (string, error)
	SignIn(auth entities.AuthRequestDTO) (string, error)
	AuthenticateByToken(token string) (int, error)
}

type User interface {
	GetUserById(id int) (entities.User, error)
	UpdateUserById(id int, dto entities.UpdateUserDTO) error
}

type Todo interface {
}

type Service struct {
	Auth
	User
	Todo
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		Auth: NewAuthService(repos.Auth),
		User: NewUserService(repos.User),
	}
}
