package service

import (
	"gitlab.com/meshnyan/todo-app/entities"
	"gitlab.com/meshnyan/todo-app/pkg/repository"
)

type UserService struct {
	repo repository.User
}

func NewUserService(repo repository.User) *UserService {
	return &UserService{repo: repo}
}

func (s *UserService) GetUserById(id int) (entities.User, error) {
	return s.repo.GetUserById(id)
}

func (s *UserService) UpdateUserById(id int, dto entities.UpdateUserDTO) error {
	return s.repo.UpdateUserById(id, dto)
}
